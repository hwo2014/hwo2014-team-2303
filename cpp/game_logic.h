#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>

using namespace std;

class game_logic
{

	struct trackdata {
		bool straight;
		double len;
		double angle;
		double radius;
		bool switc;
	};

public:
  typedef std::vector<jsoncons::json> msg_vector;

  game_logic();
  msg_vector react(const jsoncons::json& msg);

private:
  typedef std::function<msg_vector(game_logic*, const jsoncons::json&)> action_fun;
  const std::map<std::string, action_fun> action_map;

  msg_vector on_your_car(const jsoncons::json& data);
  msg_vector on_game_init(const jsoncons::json& data);
  msg_vector on_join(const jsoncons::json& data);
  msg_vector on_game_start(const jsoncons::json& data);
  msg_vector on_car_positions(const jsoncons::json& data);
  msg_vector on_crash(const jsoncons::json& data);
  msg_vector on_game_end(const jsoncons::json& data);
  msg_vector on_error(const jsoncons::json& data);

	std::vector<trackdata> tdata_;
	vector<int> lane_;	
	vector<double> max_req_speed_;
	int last_switch_index_;
	int prev_index_ = 0;
	double prev_distance_ = 0;
	double prev_velocity_ = 0;

	double max_angle = 0;
};

#endif
