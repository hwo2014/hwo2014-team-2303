#include <vector>
#include "game_logic.h"
#include "protocol.h"

using jsoncons::pretty_print;
using namespace std;
using namespace hwo_protocol;

game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error },
			{ "yourCar", &game_logic::on_your_car },
			{ "gameInit", &game_logic::on_game_init }
    }
{
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data) {
  std::cout << "on Your Car" << std::endl;
	cout << pretty_print(data) << endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data) {
  std::cout << "Game Init" << std::endl;
	cout << pretty_print(data) << endl;
	tdata_.clear();
	lane_.clear();
	last_switch_index_ = -1;
	auto pieces = data["race"]["track"]["pieces"];
	for (auto it = pieces.begin_elements(); it != pieces.end_elements(); it++) {
		trackdata tmp;
		if (it->has_member("length")) {
			tmp.straight = true;
			tmp.len = (*it)["length"].as<double>();
		} else {
			tmp.straight = false;
			tmp.angle = (*it)["angle"].as<double>();
			tmp.radius = (*it)["radius"].as<double>();
		}
		try {
			tmp.switc = (*it)["switch"].as<bool>();
		} catch (const jsoncons::json_exception& e) {
			tmp.switc = false;
		}
		tdata_.push_back(tmp);
	}

	// Lane thing
	lane_.push_back(0);

	for (int i = 1; i < tdata_.size(); i++) {
		const auto tmp = tdata_[i];
		if (tmp.switc) {
			cout << "lane has switch" << endl;
			int inc = 0;
			int outc = 0;
			for (int j = i+1; j < tdata_.size(); ++j) {
				if (tdata_[j].switc) {
					break;
				}
				if (!tdata_[j].straight) {
					if (tdata_[j].angle > 0) inc++;
					else outc++;
				}
			}
			if (inc > outc) {
				lane_.push_back(1);
			} else {
				lane_.push_back(0);
			}
		} else {
			lane_.push_back(lane_.back());
		}
	}
	for (int i = 0; i < lane_.size(); i++) {
		cout << i << ": " << lane_[i] << endl;
	}
  return { make_ping() };
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
	//return { make_throttle(0.65) };
	//std::cout << "Car position: " << std::endl;
	//std::cout << pretty_print(data) << std::endl;

	/*double xx = data[0]["piecePosition"]["inPieceDistance"].as<double>();
	if (xx > 50) {
		return { make_throttle(0.1) };
	} else {
		return { make_throttle(1.0) };
	}*/
	int cur_index = data[0]["piecePosition"]["pieceIndex"].as<int>();
	/*if (cur_index > 1) {
		return { make_throttle(0) };
	}*/

	/*double dtmp = data[0]["piecePosition"]["inPieceDistance"].as<double>();
	if (cur_index == prev_index_) {
		double vtmp = dtmp - prev_distance_;
		cout << vtmp << "\t\t" << vtmp - prev_velocity_ <<  endl;
		prev_velocity_ = vtmp;
	} else {
		prev_index_ = cur_index;
		prev_velocity_ = 0;
	}
	prev_distance_ = dtmp;

	if (data[0]["piecePosition"]["lap"].as<int>() > 0) {
		return { make_throttle(0) };
	}
	return { make_throttle(1.00) };*/



	double angle = data[0]["angle"].as<double>();
	max_angle = max(max_angle, abs(max_angle));

	int lane = data[0]["piecePosition"]["lane"]["endLaneIndex"].as<int>();
	if (cur_index + 1 < tdata_.size()) {
		if (lane != lane_[cur_index + 1] && tdata_[cur_index + 1].switc) {
			if (last_switch_index_ != cur_index) {  
				if (lane_[cur_index + 1] == 0) {
					last_switch_index_ = cur_index;
					return { make_request("switchLane", "Left") };
				} else {
					last_switch_index_ = cur_index;
					return { make_request("switchLane", "Right") };
				}
			}
		}
	}
	return { make_throttle(0.65) };

	double angle_offset = 0;
	if (abs(angle) <= 1) {
		//angle_offset = abs(angle) / 46;
	} else {
		angle_offset = 0.8;
	}
	/*} else if(abs(angle) > 45) {
		angle_offset = .5 + ((abs(angle) - 45) / 60);
	}*/

	return { make_throttle(1 - angle_offset) };
	if (angle > 25) return { make_throttle(0.4) };
	if (cur_index + 1 < tdata_.size()) {
		if (tdata_[cur_index].straight && tdata_[cur_index + 1].straight) {
			return { make_throttle(1) };
		} else if (tdata_[cur_index].straight) {
			return { make_throttle(0.45) };
		}
	}
  return { make_throttle(0.5) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  std::cout << "Someone crashed" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  std::cout << "Max Angle: " << max_angle << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}
